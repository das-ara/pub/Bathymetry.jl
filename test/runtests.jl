using Bathymetry 
using Test, Documenter

doctest(Bathymetry)

switch_database("GEBCO 2008")

@testset "Bathymetry.jl" begin
    eq(x, y) = abs(x - y) <= 150m

    @test eq(bathymetry(-23°, -38°), 3634.3329m)
    @test bathymetry((-23°, -38°)) == bathymetry(-23°, -38°)
    @test bathymetry((-23°, -38°)) == -elevation(-23°, -38°)

    loc1, loc2, loc3 = (1°, 2°), (3°, 4°), (-60°, -37°)
    @test eq(bathymetry(loc1), 4615m)
    @test eq(bathymetry(loc2), 3779m)
    @test eq(bathymetry(loc3), 2779m)
end
