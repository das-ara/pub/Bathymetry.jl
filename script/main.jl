using NCDatasets


const x = Dataset("gebco/data/GEBCO_2019.nc")
# const m = Dataset("gebco/TID/GEBCO_2019_TID.nc")

const lats = let vlat = x["lat"]
    fst  = vlat[1]
    lst  = vlat[end]
    step = vlat[2] - fst
    fst:step:lst
end
const lons = let vlon = x["lon"]
    fst  = vlon[1]
    lst  = vlon[end]
    step = vlon[2] - fst
    fst:step:lst
end


# I want to just export this function
"""
    bathymetry(lat, lon)
"""
function bathymetry(lat, lon)
   ilat, olat = findindex(lats, lat)
   ilon, olon = findindex(lons, lon)

   # Linear interpolation
   let
       bat = x["elevation"]
       y1 = bat[ilon, ilat] *     (1 - olon) + bat[ilon + 1, ilat]     * olon
       y2 = bat[ilon, ilat + 1] * (1 - olon) + bat[ilon + 1, ilat + 1] * olon

       return y1 * (1 - olat) + y2 * olat
   end
end
bathymetry((lat, lon)) = bathymetry(lat, lon)


function findindex(ran::AbstractRange, x)
    d, r = divrem(x - first(ran), step(ran))

    return Int(d) + 1, r / step(ran)
end
