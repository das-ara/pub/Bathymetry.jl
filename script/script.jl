using CSV

path = convert(Matrix, CSV.read("geod-h10-15nov.csv"; header=false))
pathold = convert(Matrix, CSV.read("geod-h10.csv"; header=false))

bats = [ bathymetry(lat, lon) for (r, lat, lon) in eachrow(path) ]
batsold = [ bathymetry(lat, lon) for (r, lat, lon) in eachrow(pathold) ]


using Plots

plot([bats batsold])


using DataFrames

CSV.write("bat-to-h10.csv", DataFrame(r = path[:, 1], bats = batsold))
CSV.write("bat-to-h10-15nov.csv", DataFrame(r = path[:, 1], bats = bats))
