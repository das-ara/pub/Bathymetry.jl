module Bathymetry

using Reexport
using NCDatasets
@reexport using Unitful
@reexport using Geo
using Unitful: m, km, °
using DataDeps

export bathymetry, elevation
export m, km, °#, switch_database


function __init__()
    global init = false
    global database = "GEBCO 2020"

    register(DataDep("bathymetry_gebco_2020",
        """
        GEBCO bathymetry database for 2020.
        """,
        "https://www.bodc.ac.uk/data/open_download/gebco/gebco_2020/zip/",
        "3b54e3c3c894ab99fc2ee47ec969d3a51095545fd85eb598a5d8a5bef47b618d";
        post_fetch_method = unpack))
    register(DataDep("bathymetry_gebco_2019",
        """
        GEBCO bathymetry database for 2019.
        """,
        "https://www.bodc.ac.uk/data/open_download/gebco/GEBCO_15SEC/zip/",
        "6b82d1b61304ba3b03fe81076a30e48cc971f749b82df72251b991b181c95d6e";
        post_fetch_method = unpack))
    register(DataDep("bathymetry_gebco_2014",
        """
        GEBCO bathymetry database for 2014.
        """,
        "https://www.bodc.ac.uk/data/open_download/gebco/GEBCO_30SEC/zip/",
        "5b609ab5ceecdc8f25f654cf47e8ffdd37a3c39f198b7d5d2a2b20604c7af458";
        post_fetch_method = unpack
        ))
    register(DataDep("bathymetry_gebco_2008",
        """
        GEBCO bathymetry database for 2008.
        """,
        "https://www.bodc.ac.uk/data/open_download/gebco/GEBCO_1MIN/zip/",
        "1088d91375b2c077a4d73f5ccc8884c2abc478c7f02a83d989ce8fc9ee16fa17";
        post_fetch_method = unpack
        ))


end


const databases = ("GEBCO 2020", "GEBCO 2019", "GEBCO 2014", "GEBCO 2008")


"""
    switch_database(db::String)

Switch the source of bathymetric data.

It should be one of $databases.
"""
function switch_database(str)
    @assert in(str, databases) "Database should be one of the following:\n$databases"

    global database = str
    global init = false

    return nothing
end


"Translate from db name to file name"
function dbname(str)
    str == "GEBCO 2020" && return "bathymetry_gebco_2020/GEBCO_2020.nc"
    str == "GEBCO 2019" && return "bathymetry_gebco_2019/GEBCO_2019.nc"
    str == "GEBCO 2014" && return "bathymetry_gebco_2014/GEBCO_2014_2D.nc"
    str == "GEBCO 2008" && return "bathymetry_gebco_2008/GRIDONE_2D.nc"
end

"Initialize the datadep and put the file path in `datapath`, as well as `lats` and `lons`"
function initialize_data()
    init && return nothing

    global datapath = @datadep_str dbname(database)
    Dataset(datapath) do x
        global lats = let vlat = x["lat"]
            fst  = vlat[1]
            lst  = vlat[end]
            step = vlat[2] - fst
            fst:step:lst
        end
        global lons = let vlon = x["lon"]
            fst  = vlon[1]
            lst  = vlon[end]
            step = vlon[2] - fst
            fst:step:lst
        end
    end
    global init = true
    return nothing
end

"""
    bathymetry(lat, lon)
    bathymetry(loc)

Return the bathymetry of a point in the ocean. It returns positive results.

It's equivalent to `elevation` but it only applies to negative-elevation points and
returns them as positive (unitful) numbers.

# Example

```jldoctest
julia> using Bathymetry

julia> bathymetry((-45°, -40°))
4993.249999901229 m
```
"""
bathymetry((lat, lon)) = bathymetry(lat, lon)
function bathymetry(lat, lon)
    bat = -elevation(lat, lon)
    @assert bat >= 0m "Point $((lat, lon)) does not belong in the ocean"

    return bat
end

"""
    elevation(lat, lon)
    elevation(loc)

Return the elevation of a point anywhere in the world. 

It returns negative results if `loc` falls in the ocean.

# Example

```jldoctest
julia> using Bathymetry

julia> elevation((-45°, -40°))
-4993.249999901229 m
```
"""
function elevation(lat, lon)
    Geo.checkgeopos((lat, lon))

    initialize_data() # Runs only once
    
    ilat, olat = findindex(lats, ustrip(lat))
    ilon, olon = findindex(lons, ustrip(lon))

    # Linear interpolation
    return Dataset(datapath) do x
        bat = x["elevation"]
        y1 = bat[ilon, ilat] *     (1 - olon) + bat[ilon + 1, ilat]     * olon
        y2 = bat[ilon, ilat + 1] * (1 - olon) + bat[ilon + 1, ilat + 1] * olon
 
        (y1 * (1 - olat) + y2 * olat) * m
    end

end
elevation((lat, lon)) = elevation(lat, lon)


function findindex(ran::AbstractRange, x)
    d, r = divrem(x - first(ran), step(ran))

    return Int(d) + 1, r / step(ran)
end


end # module
