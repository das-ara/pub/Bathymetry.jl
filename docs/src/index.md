```@meta
CurrentModule = Bathymetry
```

# Bathymetry

It exports `elevation` and `bathymetry`. 

Whereas `elevation` applies to any coordinate in the globe, `bathymetry` can only be called
in points within the ocean, where it returns positive numbers.

For a point `loc` in the ocean, it is in fact the case that `bathymetry(loc) == -elevation(loc)`.

```@index
```

```@autodocs
Modules = [Bathymetry]
Private = false
```

