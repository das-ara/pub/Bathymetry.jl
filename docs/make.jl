using Bathymetry
using Documenter

DocMeta.setdocmeta!(Bathymetry, :DocTestSetup, :(using Bathymetry); recursive=true)

makedocs(;
    modules=[Bathymetry],
    authors="Rui Rojo <rui.rojo@gmail.com>",
    repo="https://gitlab.com/das-ara/pub/Bathymetry.jl/blob/{commit}{path}#{line}",
    sitename="Bathymetry.jl",
    format=Documenter.HTML(;
        prettyurls=get(ENV, "CI", "false") == "true",
        canonical="https://das-ara.gitlab.io/pub/Bathymetry.jl",
        assets=String[],
    ),
    pages=[
        "Home" => "index.md",
    ],
)

