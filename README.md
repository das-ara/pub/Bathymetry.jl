# Bathymetry


[![Dev](https://img.shields.io/badge/docs-dev-blue.svg)](https://das-ara.gitlab.io/pub/Bathymetry.jl/dev)
[![Build Status](https://gitlab.com/das-ara/pub/Bathymetry.jl/badges/master/pipeline.svg)](https://gitlab.com/das-ara/pub/Bathymetry.jl/pipelines)
[![Coverage](https://gitlab.com/das-ara/pub/Bathymetry.jl/badges/master/coverage.svg)](https://gitlab.com/das-ara/pub/Bathymetry.jl/commits/master)


It exports `elevation` and `bathymetry`. 

Whereas `elevation` applies to any coordinate in the globe, `bathymetry` can only be called
in points within the ocean, where it returns positive numbers.

For a point `loc` in the ocean, it is in fact the case that `bathymetry(loc) == -elevation(loc)`.