using Bathymetry, CSV, DataFrames

println(1)
path = convert(Matrix, CSV.read("geod-h4-15nov.csv"; header=false))
println(2)

bats = [ bathymetry(lat, lon) for (r, lat, lon) in eachrow(path) ]

println(3)

using DataFrames

CSV.write("bat-to-h4-15nov.csv", DataFrame(r = path[:, 1], bats = bats))
println(4)
